"""HM8118 Interface Class."""

import time
from enum import Enum

import serial


class MeasurementFrequency(Enum):
    """Enum for the measurement frequency."""

    FREQ_20 = 20
    FREQ_24 = 24
    FREQ_25 = 25
    FREQ_30 = 30
    FREQ_36 = 36
    FREQ_40 = 40
    FREQ_45 = 45
    FREQ_50 = 50
    FREQ_60 = 60
    FREQ_72 = 72
    FREQ_75 = 75
    FREQ_80 = 80
    FREQ_90 = 90
    FREQ_100 = 100
    FREQ_120 = 120
    FREQ_150 = 150
    FREQ_180 = 180
    FREQ_200 = 200
    FREQ_240 = 240
    FREQ_250 = 250
    FREQ_300 = 300
    FREQ_360 = 360
    FREQ_400 = 400
    FREQ_450 = 450
    FREQ_500 = 500
    FREQ_600 = 600
    FREQ_720 = 720
    FREQ_750 = 750
    FREQ_800 = 800
    FREQ_900 = 900
    FREQ_1K = 1000
    FREQ_1K2 = 1200
    FREQ_1K5 = 1500
    FREQ_1K8 = 1800
    FREQ_2K = 2000
    FREQ_2K4 = 2400
    FREQ_2K5 = 2500
    FREQ_3K = 3000
    FREQ_3K6 = 3600
    FREQ_4K = 4000
    FREQ_4K5 = 4500
    FREQ_5K = 5000
    FREQ_6K = 6000
    FREQ_7K2 = 7200
    FREQ_7K5 = 7500
    FREQ_8K = 8000
    FREQ_9K = 9000
    FREQ_10K = 10000
    FREQ_12K = 12000
    FREQ_15K = 15000
    FREQ_18K = 18000
    FREQ_20K = 20000
    FREQ_24K = 24000
    FREQ_25K = 25000
    FREQ_30K = 30000
    FREQ_36K = 36000
    FREQ_40K = 40000
    FREQ_45K = 45000
    FREQ_50K = 50000
    FREQ_60K = 60000
    FREQ_72K = 72000
    FREQ_75K = 75000
    FREQ_80K = 80000
    FREQ_90K = 90000
    FREQ_100K = 100000
    FREQ_120K = 120000
    FREQ_150K = 150000
    FREQ_180K = 180000
    FREQ_200K = 200000


class TriggerMode(Enum):
    """Enum for the trigger mode."""

    AUTO = 0
    MANUAL = 1
    EXTERNAL = 2


class MeasurementRate(Enum):
    """Enum for measurement rates."""

    FAST = 0
    MEDIUM = 1
    SLOW = 2


class MeasurementRange(Enum):
    """Enum for measurement ranges."""

    RANGE_AUTO = 0
    RANGE_1 = 1
    RANGE_2 = 2
    RANGE_3 = 3
    RANGE_4 = 4
    RANGE_5 = 5
    RANGE_6 = 6


class MeasurementFunction(Enum):
    """Enum for measurement functions."""

    AUTO = 0
    L_Q = 1
    L_R = 2
    C_D = 3
    C_R = 4
    R_Q = 5
    Z_THETA = 6
    Y_THETA = 7
    R_X = 8
    G_B = 9
    N_THETA = 10
    M = 11


class EquivalentCircuitMode(Enum):
    """Enum for equivalent circuit modes."""

    SERIES = 0
    PARALLEL = 1
    AUTO = 2


class Device:
    """Class for interfacing with the Hameg HM8118 LCR meter."""

    def __init__(self, port: str) -> None:
        """Initialize the connection to the device."""
        self.ser = serial.Serial(port, 9600, timeout=1)

    def __del__(self) -> None:
        """Close the connection to the device."""
        self.ser.close()

    def _send_cmd(self, cmd: str) -> None:
        """Send a command to the instrument."""
        self.ser.write(cmd.encode("utf-8") + b"\r\n")

    def _read_line(self) -> str:
        """Read a line from the instrument."""
        return self.ser.readline().decode("utf-8")

    def get_id(self) -> str:
        """Get the instrument ID."""
        self._send_cmd("*IDN?")
        return self._read_line()

    def reset(self) -> None:
        """Reset the instrument."""
        self._send_cmd("*RST")

    def check_operation_complete(self) -> bool:
        """Check if the operation is complete."""
        self._send_cmd("*OPC?")
        line = self._read_line()
        return line == "1\r"

    def set_avg(self, avg: int) -> None:
        """Set the number of measurements to average over."""
        minimum_avg = 2
        maximum_avg = 99
        # check if the operation is valid
        if avg < minimum_avg:
            # deactivate averaging
            self._send_cmd("AVGM 0")
        elif avg > maximum_avg:
            msg = f"Average must be between {minimum_avg} and {maximum_avg}"
            raise ValueError(msg)
        else:
            # activate averaging
            self._send_cmd("AVGM 2")
            # set the number of measurements
            self._send_cmd(f"NAVG {avg}")

    def get_avg(self) -> int:
        """Get the number of measurements to average over."""
        no_average = 0
        med_mode = 2
        # check if averaging is active
        self._send_cmd("AVGM?")
        if int(self._read_line()) == no_average:
            # no averaging
            return 1
        if int(self._read_line()) == med_mode:
            # middle averaging mode is active 6 measurements
            return 6
        # Custom amount of averages
        self._send_cmd("NAVG?")
        return int(self._read_line())

    def set_voltage_bias(self, voltage: float) -> None:
        """Set the voltage bias."""
        # check correct range
        minimum_voltage = 0
        maximum_voltage = 5
        if voltage < minimum_voltage or voltage > maximum_voltage:
            msg = f"Voltage must be between {minimum_voltage} V and {maximum_voltage} V"
            raise ValueError(msg)
        # check if we are in a valid mode
        if self.get_measurement_function() not in [
            MeasurementFunction.C_D,
            MeasurementFunction.C_R,
            MeasurementFunction.R_X,
            MeasurementFunction.Z_THETA,
        ]:
            msg = "Voltage bias can only be used in C-D, C-R, R-X, and Z-Theta modes"
            raise ValueError(msg)
        self._send_cmd(f"VBIA {voltage}")
        self._send_cmd("BIAS 1")
        self._send_cmd("CONV 1")

    def get_voltage_bias(self) -> float:
        """Get the voltage bias."""
        self._send_cmd("VBIA?")
        return float(self._read_line())

    def set_current_bias(self, current: float) -> None:
        """Set the current bias Current must be between 0.001 A and 0.2 A."""
        minimum_current = 0.001
        maximum_current = 0.2
        # check correct range
        if current < minimum_current or current > maximum_current:
            msg = f"Current must be between {minimum_current} A and {maximum_current} A"
            raise ValueError(msg)
        # check if we are in a valid mode
        if self.get_measurement_function() not in [
            MeasurementFunction.L_Q,
            MeasurementFunction.L_R,
            MeasurementFunction.N_THETA,
            MeasurementFunction.M,
        ]:
            msg = "Current bias can only be used in L-Q, L-R, N-Theta, and M modes"
            raise ValueError(msg)
        self._send_cmd("CONV 1")
        self._send_cmd("BIAS 1")
        self._send_cmd(f"IBIA {current}")

    def get_current_bias(self) -> float:
        """Get the current bias."""
        self._send_cmd("IBIA?")
        return float(self._read_line())

    def deactivate_bias(self) -> None:
        """Deactivate the bias."""
        self._send_cmd("BIAS 0")
        self._send_cmd("CONV 0")

    def activate_external_bias(self) -> None:
        """Activate external bias."""
        self._send_cmd("BIAS 2")

    def set_equivalent_circuit_mode(self, mode: EquivalentCircuitMode) -> None:
        """Set the equivalent circuit mode."""
        self._send_cmd(f"CIRC {mode.value}")

    def get_equivalent_circuit_mode(self) -> EquivalentCircuitMode:
        """Get the equivalent circuit mode."""
        self._send_cmd("CIRC?")
        return EquivalentCircuitMode(int(self._read_line()))

    def set_frequency(self, freq: MeasurementFrequency) -> None:
        """Set the measurement frequency."""
        self._send_cmd(f"FREQ {freq.value}")

    def get_frequency(self) -> MeasurementFrequency:
        """Get the measurement frequency."""
        self._send_cmd("FREQ?")
        return MeasurementFrequency(int(self._read_line()))

    def set_trigger_mode(self, mode: TriggerMode) -> None:
        """Set the trigger mode."""
        self._send_cmd(f"MMOD {mode.value}")

    def get_trigger_mode(self) -> TriggerMode:
        """Get the trigger mode."""
        self._send_cmd("MMOD?")
        return TriggerMode(int(self._read_line()))

    def trigger(self) -> None:
        """Trigger a measurement."""
        self._send_cmd("*TRG")

    def set_measurement_rate(self, rate: MeasurementRate) -> None:
        """Set the measurement rate."""
        self._send_cmd(f"RATE {rate.value}")

    def get_measurement_rate(self) -> MeasurementRate:
        """Get the measurement rate."""
        self._send_cmd("RATE?")
        return MeasurementRate(int(self._read_line()))

    def set_measurement_range(self, m_range: MeasurementRange) -> None:
        """Set the measurement range."""
        # check if the value is AUTO
        if m_range == MeasurementRange.RANGE_AUTO:
            self._send_cmd("RNGH 0")
            return
        # set the range
        self._send_cmd(f"RNGE {m_range.value}")

    def get_measurement_range(self) -> MeasurementRange:
        """Get the measurement range."""
        # check if the value is AUTO
        self._send_cmd("RNGH?")
        if int(self._read_line()) == 0:
            return MeasurementRange.RANGE_AUTO
        self._send_cmd("RNGE?")
        return MeasurementRange(int(self._read_line()))

    def set_measurement_function(self, function: MeasurementFunction) -> None:
        """Set the measurement function."""
        self._send_cmd(f"PMOD {function.value}")

    def get_measurement_function(self) -> MeasurementFunction:
        """Get the measurement function."""
        self._send_cmd("PMOD?")
        return MeasurementFunction(int(self._read_line()))

    def set_measurement_voltage(self, voltage: float) -> None:
        """Set the measurement voltage."""
        minimum_voltage = 0.05
        maximum_voltage = 1.5
        # check valid range
        if voltage < minimum_voltage or voltage > maximum_voltage:
            msg = f"Voltage must be between {minimum_voltage} V and {maximum_voltage} V"
            raise ValueError(msg)
        self._send_cmd(f"VOLT {voltage}")

    def get_measurement_voltage(self) -> float:
        """Get the measurement voltage."""
        self._send_cmd("VOLT?")
        return float(self._read_line())

    def calibrate_on_current_frequency(self) -> None:
        """Calibrate on the current frequency."""
        self._send_cmd("CALL 0")
        self._send_cmd("CROP")
        # check if the result is 0
        if int(self._read_line()) != 0:
            msg = "Calibration failed on Open test"
            raise ValueError(msg)

        self._send_cmd("CALL 0")
        self._send_cmd("CRSH")
        # check if the result is 0
        if int(self._read_line()) != 0:
            msg = "Calibration failed on Short test"
            raise ValueError(msg)

    def calibrate_on_all_frequencies(self) -> None:
        """Calibrate on all frequencies."""
        self._send_cmd("CALL 1")
        self._send_cmd("CROP")
        # check if the result is 0
        if int(self._read_line()) != 0:
            msg = "Calibration failed on Open test"
            raise ValueError(msg)

        self._send_cmd("CALL 1")
        self._send_cmd("CRSH")
        # check if the result is 0
        if int(self._read_line()) != 0:
            msg = "Calibration failed on Short test"
            raise ValueError(msg)

    def get_complete_measurement(self) -> tuple[float, float, int]:
        """Get the complete measurement."""
        self._send_cmd("XALL?")
        return_str = self._read_line()
        # split the string
        return_list = return_str.split(",")
        return float(return_list[0]), float(return_list[1]), int(return_list[2])

    def get_primary_measurement(self) -> float:
        """Get the primary measurement."""
        self._send_cmd("XMAJ?")
        return float(self._read_line())

    def get_secondary_measurement(self) -> float:
        """Get the secondary measurement."""
        self._send_cmd("XMIN?")
        return float(self._read_line())

    def wait_for_complete(self) -> None:
        """Wait for the measurement to complete."""
        while not self.check_operation_complete():
            time.sleep(0.1)

    def take_measurement(self) -> tuple[float, float]:
        """Take a measurement."""
        self.trigger()
        self.wait_for_complete()
        return self.get_primary_measurement(), self.get_secondary_measurement()
