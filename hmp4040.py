"""HMP4040 power supply driver."""

from __future__ import annotations

from enum import Enum

import serial


class Channel(Enum):
    """Channel selection."""

    CHANNEL1 = "1"
    CHANNEL2 = "2"
    CHANNEL3 = "3"
    CHANNEL4 = "4"


class Voltage(Enum):
    """Voltage selection."""

    MINIMUM = "MIN"
    MAXIMUM = "MAX"
    UP = "UP"
    DOWN = "DOWN"


class MinMax(Enum):
    """Minimum, Maximum selection."""

    MAXIMUM = "MAX"
    MINIMUM = "MIN"


class DefMinMax(Enum):
    """Default, Maximum, Minimum selection."""

    DEFAULT = "DEF"
    MAXIMUM = "MAX"
    MINIMUM = "MIN"


class OnOff(Enum):
    """On/Off selection."""

    ON = "ON"
    OFF = "OFF"


class OvervoltageMode(Enum):
    """Overvoltage protection mode selection."""

    MEASURED = "MEAS"
    PROTECTED = "PROT"


class ScpiError(Enum):
    """System Error States."""

    NO_ERROR = 0
    COMMAND_ERROR = -100
    SYNTAX_ERROR = -102
    QUEUE_OVERFLOW = -350


class Device:
    """HMP4040 power supply driver."""

    def __init__(self, port: str) -> None:
        """Initialize the device.

        Open Serial connection to the device.
        """
        self.ser = serial.Serial(port, 9600, timeout=1)

    def __del__(self) -> None:
        """Close the serial connection."""
        self.ser.close()

    def _send_cmd(self, cmd: str) -> None:
        """Send a command to the instrument."""
        self.ser.write(cmd.encode("utf-8") + b"\r\n")

    def _read_line(self) -> str:
        """Read a line from the instrument."""
        return self.ser.readline().decode("utf-8")

    def clear_status(self) -> None:
        """Clear Status Buffer.

        Sets the status byte (STB), the standard event register (ESR),
        the EVENt part of the QUEStionable and the OPERation registers to zero.
        The command does not alter the mask and transition parts of the registers.
        It clears the output buffer.
        """
        self._send_cmd("*CLS")

    def event_status_enable(self, status: int) -> int:
        """Set the event status enable register to the specified value.

        The query returns the contents of the event status enable register
        in decimal form.

        @param status:  Event status enable register in decimal form.
                        Bit 6 (MSS mask bit) is always 0.

        """
        maximum = 255
        # check if status is in range (0-255)
        if not 0 <= status <= maximum:
            msg = f"status must be in range 0-{maximum}"
            raise ValueError(msg)
        self._send_cmd(f"*ESE {status}")
        return int(self._read_line())

    def check_event_status(self) -> int:
        """Check the event status register.

        Returns the contents of the event status register in decimal form
        and then sets the register to zero
        """
        self._send_cmd("*ESR?")
        return int(self._read_line())

    def get_id(self) -> str:
        """Get the instrument identification."""
        self._send_cmd("*IDN?")
        return self._read_line()

    def check_operation_complete(self) -> bool:
        """Check if all preceding commands have been executed.

        Sets bit 0 in the event status register when all preceding commands have
        been executed. This bit can be used to initiate a service request. The query
        writes a "1" into the output buffer when all preceding commands have been
        executed, which is useful for command synchronization.
        """
        # TODO(Alex): Check if this is the correct command
        # 000
        self._send_cmd("*OPC")
        return bool(int(self._read_line()))

    def reset(self) -> None:
        """Reset the instrument.

        Sets the instrument to a defined default status.
        The default settings are indicated in the description of commands.
        """
        self._send_cmd("*RST")

    def service_request_enable(self, status: int) -> int:
        """Set Service Request Enable Register.

        Sets the service request enable register to the indicated value.
        This command determines under which conditions a service request is triggered.

        @param status:  Contents of the service request enable register in decimal form.
                        Bit 6 (MSS mask bit) is always 0.
                        Range: 0 to 255
                        # TODO
        """
        maximum = 255
        # check if status is in range (0-255)
        if not 0 <= status <= maximum:
            msg = f"status must be in range 0-{maximum}"
            raise ValueError(msg)
        self._send_cmd(f"*SRE {status}")
        return int(self._read_line())

    def check_status_byte(self) -> int:
        """Read the contents of the status byte in decimal form."""
        self._send_cmd("*STB?")
        return int(self._read_line())

    def self_test(self) -> tuple[bool, int]:
        """Execute a self-test of the instrument."""
        self._send_cmd("*TST?")
        ret_val = self._read_line()
        return bool(int(ret_val)), int(ret_val)

    def wait_to_continue(self) -> None:
        """Wait before continuing.

        Prevents servicing of the subsequent commands until all preceding commands have
        been executed and all signals have settled.
        """
        self._send_cmd("*WAI")

    def save_settings(self, slot: int) -> None:
        """Save the current instrument settings.

        Stores the current instrument settings under the specified number in an internal
        memory.
        The settings can be recalled using the command *RCL with the associated number.

        @param slot:    Number of the memory slot in which the settings are to be stored
                        Range: 0 to 9
        """
        maximum_slot = 9
        if not 0 <= slot <= maximum_slot:
            msg = f"slot must be in range 0-{maximum_slot}"
            raise ValueError(msg)
        self._send_cmd(f"*SAV {slot}")

    def recall_settings(self, slot: int) -> None:
        """Load previously saved instrument settings.

        Loads the instrument settings from an internal memory identified by the
        specified number.
        The instrument settings can be stored to this memory,
        using the command *SAV with the associated number

        @param slot:    Number of the memory slot from which the settings are to be
                        recalled.
                        Range: 0 to 9
        """
        maximum_slot = 9
        # check if slot is in range (0-9)
        if not 0 <= slot <= maximum_slot:
            msg = f"slot must be in range 0-{maximum_slot}"
            raise ValueError(msg)
        self._send_cmd(f"*RCL {slot}")

    def select_channel(self, channel: Channel) -> None:
        """Select the channel to be controlled."""
        self._send_cmd(f"INST {channel}")

    def set_voltage(self, voltage: float | Voltage) -> None:
        """Set the voltage value of the selected channel.

        @param voltage: Output voltage in volts.
                        Range: 0.000 to 32.050
                        Increment 0.001
                        Default unit: V
        """
        self._send_cmd(f"VOLT {voltage}")

    def set_voltage_step(self, step: float) -> None:
        """Set the voltage step size for the selected channel.

        @param step:    Voltage step size in volts.
                        Range: 0.000 to 32.050
                        Default unit: V
        """
        self._send_cmd(f"VOLT:STEP {step}")

    def set_current(self, current: float) -> None:
        """Set the current value of the selected channel.

        @param current: Output current in amperes.
                        Range: 0.000 to 10.000
                        Default unit: A
        """
        maximum_current = 10.0
        # check if current is in range (0-10)
        if not 0.0 <= current <= maximum_current:
            msg = f"current must be in range 0.0-{maximum_current}"
            raise ValueError(msg)
        self._send_cmd(f"CURR {current}")

    def set_current_step(self, step: float) -> None:
        """Set the current step size for the selected channel.

        @param step:    Current step size in amperes.
                        Range: 0.001 to 10.010
                        Default unit: A
        """
        minimum_step = 0.001
        maximum_step = 10.010
        # check if step is in range (0.001-10.010)
        if not minimum_step <= step <= maximum_step:
            msg = f"step must be in range {minimum_step}-{maximum_step}"
            raise ValueError(msg)
        self._send_cmd(f"CURR:STEP {step}")

    def set_voltage_and_current(
        self,
        voltage: float | DefMinMax,
        current: float | DefMinMax,
    ) -> None:
        """Set voltage and current of the selected channel.

        The APPLy subsystem provides a command that enables you to set the current and
        voltage of a channel in one step.

        The combined voltage and current setting command takes approximately 100 ms,
        i.e. longer than the setting of a single value

        @param voltage: Output voltage in volts.
                        Range: 0.000 to 32.050
                        Increment 0.001
                        Default unit: V

        @param current: Output current in amperes.
                        Range: 0.000 to 10.000
                        Default unit: A
        """
        self._send_cmd(f"APPL {voltage},{current}")

    def set_output_general(self, state: OnOff | bool) -> None:
        """Turn on all selected channels simultaneously.

        @param state:   ON, OFF, True, False
        """
        if isinstance(state, bool):
            state = OnOff.ON if state else OnOff.OFF
        self._send_cmd(f"OUTP:GEN {state.value}")

    def select_output(self, state: OnOff | bool) -> None:
        """Activates the selected channel.Activates the selected channel.

        @param state:   ON, OFF, True, False
        """
        if isinstance(state, bool):
            state = OnOff.ON if state else OnOff.OFF
        self._send_cmd(f"OUTP:SEL {state.value}")

    def set_output(self, state: OnOff | bool) -> None:
        """Activates the selected channel and turns on the output.

        @param state:   ON, OFF, True, False
        """
        if isinstance(state, bool):
            state = OnOff.ON if state else OnOff.OFF
        self._send_cmd(f"OUTP {state.value}")

    # & Over-current Protection Subsystem

    def set_fuse_delay(self, delay: int | MinMax) -> None:
        """Set the fuse delay time.

        @param delay:   Fuse delay time in seconds.
                        Range: 0 to 250
                        Increment 10
                        Default unit: s
        """
        # check if delay is in range (0-10)
        if isinstance(delay, MinMax):
            self._send_cmd(f"FUSE:DEL {delay}")
            return
        maximum_delay = 250
        if not 0 <= delay <= maximum_delay:
            msg = f"delay must be in range 0-{maximum_delay}"
            raise ValueError(msg)
        self._send_cmd(f"FUSE:DEL {delay}")

    def fuse_link(self, channel: Channel) -> None:
        """Combine the fuses of several selected channels (fuse linking).

        @param channel: Channel selection
        """
        self._send_cmd(f"FUSE:LINK {channel}")

    def check_fuse_tripped(self) -> bool:
        """Check if the fuse has tripped in the selected channel.

        Queries whether the fuse has tripped in the selected channel.
        """
        self._send_cmd("FUSE:TRIP?")
        return bool(int(self._read_line()))

    def fuse_unlink(self, channel: Channel) -> None:
        """Dissolves linked fuses.

        @param channel: Channel selection
        """
        self._send_cmd(f"FUSE:UNL {channel}")

    def set_fuse(self, state: OnOff | bool) -> None:
        """Activates the fuse function in the selected channel.

        @param state:   ON, OFF, True, False
        """
        if isinstance(state, bool):
            state = OnOff.ON if state else OnOff.OFF
        self._send_cmd(f"FUSE {state}")

    # & Overvoltage Protection Subsystem

    def set_overvoltage_protection_value(self, value: float | MinMax) -> None:
        """Set the overvoltage protection value of the selected channel.

        @param value:   Overvoltage protection value in volts.
                        Range: 0.1 to 32.5
                        Increment 0.1
                        Default unit: V
        """
        if isinstance(value, MinMax):
            self._send_cmd(f"VOLT:PROT {value}")
            return
        minimum_voltage = 0.1
        maximum_voltage = 32.5
        if not minimum_voltage <= value <= maximum_voltage:
            msg = f"value must be in range {minimum_voltage}-{maximum_voltage}"
            raise ValueError(msg)
        self._send_cmd(f"VOLT:PROT {value}")

    def reset_overvoltage_protection(self) -> None:
        """Reset the overvoltage protection of the selected channel."""
        self._send_cmd("VOLT:PROT:CLE")

    def set_overvoltage_protection_mode(self, mode: OvervoltageMode) -> None:
        """Set the overvoltage protection mode of the selected channel.

        @param mode:    Overvoltage protection mode selection
        """
        self._send_cmd(f"VOLT:PROT:MODE {mode}")

    def check_overvoltage_protection_tripped(self) -> bool:
        """Check if the overvoltage protection has tripped in the selected channel.

        Queries whether the overvoltage protection has tripped in the selected channel.
        """
        self._send_cmd("VOLT:PROT:TRIP?")
        return bool(int(self._read_line()))

    # & Measurement Subsystem

    def measure_current(self) -> float:
        """Measures the current of the selected channel."""
        self._send_cmd("MEAS:CURR?")
        return float(self._read_line())

    def measure_voltage(self) -> float:
        """Measures the voltage of the selected channel."""
        self._send_cmd("MEAS:VOLT?")
        return float(self._read_line())

    # & Arbitrary Waveform Subsystem

    def clear_arbitrary_waveform(self, channel: Channel) -> None:
        """Delete the arbitrary waveform data of the selected channel.

        @param channel: Channel selection
        """
        self._send_cmd(f"ARB:C {channel}")

    def set_arbitrary_waveform(self, data: list[tuple[float, float, float]]) -> None:
        """Set the arbitrary waveform data of the selected channel.

        @param data:    Arbitrary waveform data
        """
        maximum_data_length = 128
        # Check list length
        if len(data) > maximum_data_length:
            msg = f"data list length must be less than {maximum_data_length}"
            raise ValueError(msg)
        voltage_range = (0, 32.05)
        current_range = (0.001, 10.0)
        time_range = (0.01, 60)
        # Check Voltage, current and time values in valid range
        for voltage, current, time in data:
            if voltage < voltage_range[0] or voltage > voltage_range[1]:
                msg = f"voltage must be in range {voltage_range}"
                raise ValueError(msg)
            if current < current_range[0] or current > current_range[1]:
                msg = f"current must be in range {current_range}"
                raise ValueError(msg)
            if time < time_range[0] or time > time_range[1]:
                msg = f"time must be in range {time_range}"
                raise ValueError(msg)
        # ~ All values are valid
        # ~ create command string
        cmd: str = "ARB:DATA "
        for voltage, current, time in data:
            cmd += f"{voltage},{current},{time},"

        # remove last comma
        cmd = cmd[:-1]

        self._send_cmd(cmd)

    def set_arbitrary_waveform_repeat(self, repeat: int) -> None:
        """Set the arbitrary waveform repeat count.

        @param repeat:  Arbitrary waveform repeat count.
                        Range: 0 to 255
        """
        maximum_repeat = 255
        # check if repeat is in range (0-255)
        if not 0 <= repeat <= maximum_repeat:
            msg = f"repeat must be in range 0-{maximum_repeat}"
            raise ValueError(msg)
        self._send_cmd(f"ARB:REP {repeat}")

    def get_arbitrary_waveform_repeat(self) -> int:
        """Return the arbitrary waveform repeat count."""
        self._send_cmd("ARB:REP?")
        return int(self._read_line())

    def load_arbitrary_waveform(self, memory: int) -> None:
        """Load the arbitrary waveform data from the internal memory.

        @param memory:  Arbitrary waveform memory number.
                        Range: 1 to 3
        """
        valid_memory = [1, 2, 3]
        # check if memory is in range (1-3)
        if memory not in valid_memory:
            msg = f"memory must be in a valid memory number {valid_memory}"
            raise ValueError(msg)
        self._send_cmd(f"ARB:REST {memory}")

    def save_arbitrary_waveform(self, memory: int) -> None:
        """Save the arbitrary waveform data to the internal memory.

        @param memory:  Arbitrary waveform memory number.
                        Range: 1 to 3
        """
        valid_memory = [1, 2, 3]
        # check if memory is in range (1-3)
        if memory not in valid_memory:
            msg = f"memory must be in a valid memory number {valid_memory}"
            raise ValueError(msg)
        self._send_cmd(f"ARB:SAVE {memory}")

    def start_arbitrary_waveform(self, channel: Channel) -> None:
        """Start the arbitrary waveform of the selected channel."""
        self._send_cmd(f"ARB:START {channel}")

    def stop_arbitrary_waveform(self, channel: Channel) -> None:
        """Stop the arbitrary waveform of the selected channel."""
        self._send_cmd(f"ARB:STOP {channel}")

    def transfer_arbitrary_waveform(self, channel: Channel) -> None:
        """Transfers the arbitrary waveform data to the selected channel."""
        self._send_cmd(f"ARB:TRAN {channel}")

    # & System Subsystem

    def beep(self) -> None:
        """Return a single beep immediately."""
        self._send_cmd("SYST:BEEP")

    def get_error(self) -> ScpiError:
        """Check the error/event queue of the oldest item and remove it.

        Queries the error/event queue of the oldest item and removes it from the queue.
        The response is an error number and a short description of the error.
        Positive error numbers are instrument-dependent. Negative error numbers are
        reserved by the SCPI standard.
        If the queue is empty the response is 0, "No error"
        """
        self._send_cmd("SYST:ERR?")
        return ScpiError(int(self._read_line()))

    def enable_mix_control(self) -> None:
        """Enable remote control without locking manual control.

        You can control the instrument in both modes simultaneously (mixed mode)
        """
        self._send_cmd("SYST:MIX")

    def enable_remote_control(self) -> None:
        """Set the system to remote state.

        The manual (front panel) control is locked.
        To unlock manual control, press the Remote key.
        """
        self._send_cmd("SYST:REM")

    def lock_front_panel(self) -> None:
        """Lock the front panel (manual) control.

        You can operate the instrument via remote control only. To return to manual
        control, use the command SYSTem:LOCal on page 104.
        The Remote key is locked also.
        """
        self._send_cmd("SYST:RWL")

    def unlock_front_panel(self) -> None:
        """Enable manual (front panel) control.

        The command switches from remote control to manual control, and is required,
        when you have locked manual control before, see SYSTem:RWLock on page 105.

        """
        self._send_cmd("SYST:LOC")

    def get_system_version(self) -> str:
        """Get system version.

        Queries the SCPI version the instrument's command set complies with.
        """
        self._send_cmd("SYST:VERS?")
        return self._read_line()
